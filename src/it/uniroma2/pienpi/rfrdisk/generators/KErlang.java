package it.pierdiccapizzi.generators;

import java.io.Serializable;


/** 
 * Implementa un generatore di numeri pseudo casuali con distribuzione K-Erlangiana
 * 
 * @author Pierdicca Luca
 * @author Pizzi Massimo
 */
public class KErlang extends Generator implements Serializable {
	
	private static final long serialVersionUID = 1;
	Exponential exponential;
	double parameter;

	
	
	public KErlang(int seme, double media, double parametro) {
		
		super(seme, media);
		parameter = parametro;
		exponential = new Exponential(seed, average/parameter);
		type = GeneratorType.Kerlang;
	}
	
	
	@Override
	public double generateNextValue(){
		double sum = 0L;
		
		for(int i = 1; i <= parameter; i++)
			sum = sum + exponential.generateNextValue();		//sfruttiamo la proprietÓ dell'erlangiana, somma di k esponenziali ciascuna con media Ts/k
		
		return sum;
	}

	
	@Override
	public void setAverage(double media) {
		average = media;
		exponential.average = media;
	}
}
