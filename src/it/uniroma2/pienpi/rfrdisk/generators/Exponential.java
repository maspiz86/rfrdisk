package it.pierdiccapizzi.generators;

import java.io.Serializable;


/** 
 * Implementa un generatore di numeri pseudo casuali con distribuzione esponenziale
 * 
 * @author Pierdicca Luca
 * @author Pizzi Massimo
 */
public class Exponential extends Generator implements Serializable {
	
	private static final long serialVersionUID = 1;
	RandomSequence linCon;
	Uniform unif;
	
	
	
	public Exponential(int seme, double media){

		super(seme, media);
		linCon = new RandomSequence(seme);
		unif = new Uniform(seme, linCon);
		type = GeneratorType.Exponential;
	}

	
	@Override
	public double generateNextValue(){
		return -this.average * Math.log(unif.generateNextValue());	
	}
}
