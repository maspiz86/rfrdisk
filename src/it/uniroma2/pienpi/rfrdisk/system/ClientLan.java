package it.pierdiccapizzi.system;


import java.io.Serializable;
import it.pierdiccapizzi.eventssequentiator.Clock;
import it.pierdiccapizzi.eventssequentiator.Job;
import it.pierdiccapizzi.eventssequentiator.Scheduler;
import it.pierdiccapizzi.generators.HyperExponential;



/** 
 * Implementa una LAN lato client
 * 
 * @author Pierdicca Luca
 * @author Pizzi Massimo
 */
public class ClientLan extends Network implements Serializable
{	
	private static final long serialVersionUID = 1;
	double parameterIperExp;
	HyperExponential lanClLaw;
	public static int specificCounter = 0;
	Job lastJob;


	
	public ClientLan()
	{	
		super();
		
	
		typeId = Parameters.CLANID;
		specificId = specificCounter;
		specificCounter++;
	
		type = CenterType.LanCL;
		lawType = CenterLawType.Iperesponenziale;
		
		parameterIperExp = Parameters.CLANPAR;
		lanClLaw = new HyperExponential(seed, lawAverage, parameterIperExp);	
		
		System.out.println(this.getType() + "\t\t\tId: " + typeId + " ; " + specificId);
		System.out.println("Seme " + this.seed + "\t\tIn " + this.attemptNum + " tentativi\n");
	}

	
	
	@Override
	public double calcCenterDelayTime()
	{	
		double d = lanClLaw.generateNextValue();
		if(Parameters.stampa)System.out.println(this.type+"\t"+d);
		return d;
	}

	
	@Override
	public void endOfService(Framework impianto)
	{	
		
		Center nextCenter;
		currentJob = servants[Scheduler.specificId];
		
		if(this.currentJob.isOutwarding())		//se il job � in andata
		{	
			nextCenter = impianto.getCenter(typeId+1, 0);		//il prossimo centro � il gateway lato client
			
			if(nextCenter.isOccupied){		//se il gateway � occupato
				((Gateway) nextCenter).queue.add(this.currentJob);		//mettere il job in coda
			}
			else		//se il gateway � libero
			{	
				nextCenter.setCurrentJob(this.currentJob);		//mettere il job in servizio
				nextCenter.setNewTime(Clock.time + nextCenter.calcCenterDelayTime());			//aggiornare il calendario calcolando il tempo di servizio
			}
		}
		else		//se il job � di ritorno
		{	
			nextCenter = impianto.getCenter(typeId-2, currentJob.getId());		//il prossimo centro � il client processor
			
			
			((Terminal)nextCenter).jobApproach();
	
		}	
		lastJob = this.currentJob;
		this.servants[currentJob.getId()] = null;		//liberare il servente della client lan
		this.setNewTime(currentJob.getId(), Parameters.INFINITE);
	}
	
	
	/**
	 * Restituisce l'ultimo job uscito dal centro
	 * 
	 * @return lastJob
	 */
	public Job getLastJob() {
		return lastJob;
	}

	
}