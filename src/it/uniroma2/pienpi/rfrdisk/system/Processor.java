package it.pierdiccapizzi.system;

import java.io.Serializable;


/** 
 * Rappresenta un generico Pc dell'impianto
 *  
 * @author Pierdicca Luca
 * @author Pizzi Massimo
 */
public abstract class Processor extends Center implements Serializable {

	private static final long serialVersionUID = 1;
	
	
	
	public Processor(){	
		super();
	}
	
	
	@Override
	public abstract void endOfService(Framework impianto);

	
	@Override
	public abstract double calcCenterDelayTime();

}
