package it.pierdiccapizzi.eventssequentiator;

import java.io.Serializable;
import java.util.ArrayList;
import it.pierdiccapizzi.system.Parameters;

/**
 * La classe EventsCalendar rappresenta il concetto di Calendario in simulazione.
 * Contiene tutte le occorrenze di eventi espressi sotto forma di tempi da intendere 
 * come tempi di scadenza.
 * 
 * @author Pierdicca Luca
 * @author Pizzi Massimo
 */
/* 
 * +++++++++++++++++++++++STRUTTURA CALENDARIO+++++++++++++++++++++++++++
 * |																	|
 * |	type											            	|
 * |	 T  | 0 |-->|0|-|1|-|2|-|3|-|4|-|5|-|6|-|7|-|8|-|9|-|10|-|11|	|													
 * |	 cP | 1 |-->|0|-|1|-|2|-|3|-|4|-|5|-|6|-|7|-|8|-|9|-|10|-|11|	|													
 * |	 cL | 2 |-->|0|-|1|-|2|-|3|-|4|-|5|-|6|-|7|-|8|-|9|-|10|-|11|   |
 * |	 cG | 3 |-->|0|													|
 * |	 W  | 4 |-->|0|-|1|-|2|-|3|-|4|-|5|-|6|-|7|-|8|-|9|-|10|-|11|   |
 * |	 sG | 5 |-->|0|													|
 * |	 sL | 6 |-->|0|-|1|-|2|-|3|-|4|-|5|-|6|-|7|-|8|-|9|-|10|-|11|	|
 * |	 sP | 7 |-->|0|-|1|-|2|											|
 * |	 D  | 8 |-->|0|-|1|-|2|											|
 * |																	|
 * |      [] Array														|
 * |      | | ArrayList													|
 * +--------------------------------------------------------------------+
 */
public class EventsCalendar implements Serializable {	
	
	private static final long serialVersionUID = 1;

	/**
	 * @serial
	 */
	public ArrayList<ArrayList> centerType = new ArrayList<ArrayList>(); 
	ArrayList<Double> termEventValue = new ArrayList<Double>();
	ArrayList<Double> cCpuEventValue = new ArrayList<Double>();
	ArrayList<Double> cLanEventValue = new ArrayList<Double>();
	ArrayList<Double> cGwEventValue = new ArrayList<Double>();
	ArrayList<Double> wanEventValue = new ArrayList<Double>();
	ArrayList<Double> sGwEventValue = new ArrayList<Double>();
	ArrayList<Double> sLanEventValue = new ArrayList<Double>();
	ArrayList<Double> sCpuEventValue = new ArrayList<Double>();
	ArrayList<Double> sDiskEventValue = new ArrayList<Double>();
	
	

	/**
	 * Costruisce un tipo EventsCalendar e inizializza tutti i tempi ad "infinito".
	 */
	public EventsCalendar()
	{	
		//inizializzo di default tutti i valori dei tempi di evento ad infinito
		for(int k=0 ; k<Parameters.clientNum ; k++) termEventValue.add(Parameters.INFINITE);
		for(int k=0 ; k<Parameters.clientNum ; k++) cCpuEventValue.add(Parameters.INFINITE);	 
		for(int k=0 ; k<Parameters.clientNum ; k++) cLanEventValue.add(Parameters.INFINITE);
		for(int k=0 ; k<Parameters.CGWEVENTNUM ; k++) cGwEventValue.add(Parameters.INFINITE); 
		for(int k=0 ; k<Parameters.clientNum ; k++) wanEventValue.add(Parameters.INFINITE);
		for(int k=0 ; k<Parameters.SGWEVENTNUM ; k++) sGwEventValue.add(Parameters.INFINITE);
		for(int k=0 ; k<Parameters.clientNum ; k++) sLanEventValue.add(Parameters.INFINITE);
		for(int k=0 ; k<Parameters.SCPUEVENTNUM ; k++) sCpuEventValue.add(Parameters.INFINITE);
		for(int k=0 ; k<Parameters.SDISKEVENTNUM ; k++) sDiskEventValue.add(Parameters.INFINITE);
		
		centerType.add(termEventValue);
		centerType.add(cCpuEventValue);
		centerType.add(cLanEventValue);
		centerType.add(cGwEventValue);
		centerType.add(wanEventValue);
		centerType.add(sGwEventValue);
		centerType.add(sLanEventValue);
		centerType.add(sCpuEventValue);
		centerType.add(sDiskEventValue);
		
	}
	
	
	
	/**
	 * Aggiunge un nuovo centro di tipo Istant Service.
	 */
	public void addISCenterEvent(){
	
			termEventValue.add(Parameters.INFINITE);
			cCpuEventValue.add(Parameters.INFINITE);	
			cLanEventValue.add(Parameters.INFINITE);
			wanEventValue.add(Parameters.INFINITE);
			sLanEventValue.add(Parameters.INFINITE);	
	}
		
	
	
	/**
	 * Resetta il Calendario e il Clock riassegnando i valori di partenza.
	 */
	public void reset()
	{
		int currentEventNum = 0;
		
		for (int i=0;i<centerType.size();i++){
			
			centerType.get(i).clear();
			
			switch(i){
				case 0 : currentEventNum = Parameters.clientNum; break;
				case 1 : currentEventNum = Parameters.clientNum; break;
				case 2 : currentEventNum = Parameters.clientNum; break;
				case 3 : currentEventNum = Parameters.CGWEVENTNUM; break;
				case 4 : currentEventNum = Parameters.clientNum; break;
				case 5 : currentEventNum = Parameters.SGWEVENTNUM; break;
				case 6 : currentEventNum = Parameters.clientNum; break;
				case 7 : currentEventNum = Parameters.SCPUEVENTNUM; break;
				case 8 : currentEventNum = Parameters.SDISKEVENTNUM; break;
			}
			
			for(int j=0;j<currentEventNum;j++) centerType.get(i).add(Parameters.INFINITE);
		}

		Clock.time = 0;
	}
}
