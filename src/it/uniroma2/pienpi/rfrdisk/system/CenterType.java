package it.pierdiccapizzi.system;

import java.io.Serializable;


/** 
 * Descrive la tipologia dell'unit� funzionale
 * 
 * @author Pierdicca Luca
 * @author Pizzi Massimo
 */
public enum CenterType implements Serializable {
	
	Terminale, ProcessoreCL, ProcessoreSE, LanCL, LanSE, GatewayCL, GatewaySE, Wan, Disk
}