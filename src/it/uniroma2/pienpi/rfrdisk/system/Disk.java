package it.pierdiccapizzi.system;

import java.io.PrintWriter;
import java.io.Serializable;
import java.util.LinkedList;
import it.pierdiccapizzi.eventssequentiator.Clock;
import it.pierdiccapizzi.eventssequentiator.Job;
import it.pierdiccapizzi.generators.KErlang;
import it.pierdiccapizzi.generators.RandomSequence;
import it.pierdiccapizzi.generators.Uniform;



/** 
 * Implementa una Disk lato server
 * 
 * @author Pierdicca Luca
 * @author Pizzi Massimo
 */
public class Disk extends Center implements Queuable, Serializable {
	
	private static final long serialVersionUID = 1;
	double parameterKerl;
	KErlang dLaw;
	public static int specificCounter = 0;
	Job lastJob;
	
	LinkedList<Job> queue;
	RandomSequence linConRandomAccess;
	Uniform unif;
	PrintWriter out;
	
	
	
	public Disk(Framework sistema)
	{	
		super();
		
		linConRandomAccess = new RandomSequence(this.seed);
		unif = new Uniform(this.seed, linConRandomAccess);
		queue = new LinkedList<Job>();
		
		typeId = Parameters.DISKID;
		
		specificId = specificCounter;
		specificCounter ++;
		
		type = CenterType.Disk;
		lawType = CenterLawType.Kerlangiana;
		lawAverage = Parameters.DISKAVG;
		
		parameterKerl = Parameters.DISKPAR;
		dLaw = new KErlang(seed, lawAverage, parameterKerl);	
		
		System.out.println(this.getType() + "\t\t\tId: " + typeId + " ; " + specificId);
		System.out.println("Seme " + this.seed + "\t\tIn " + this.attemptNum + " tentativi\n");
	}
	
	@Override
	public Job getPendingJob() {		//Disciplina RAND
		
		double temp = unif.generateNextValue();
		int randomIndex = (int) (temp*this.queue.size()); 
		Job randomJob = queue.get(randomIndex);
		this.queue.remove(randomIndex);
		
		return randomJob;
	}
	
	
	@Override
	public double calcCenterDelayTime()
	{
		double d = dLaw.generateNextValue();
		if(Parameters.stampa)System.out.println(this.type+"\t"+d);
		return d;
	}


	@Override
	public void endOfService(Framework impianto)
	{	
		Center nextCenter = impianto.getCenter(typeId-1, specificId); // il prossimo centro � comunque il server processor
			
		if(nextCenter.isOccupied)		//se il server processor � occupato...
			((ServerProcessor) nextCenter).queue.add(currentJob);		//...mandare il job in questione in coda
		else		//se il server processor � libero...
		{	
			nextCenter.setCurrentJob(this.currentJob);		//...servire direttamente il job
			nextCenter.setNewTime(Clock.time + nextCenter.calcCenterDelayTime());		//...aggiornare il calendario calcolando il tempo di servizio	
		}
		
	
		lastJob = currentJob;
		this.setCurrentJob(null);
		
		if(this.queue.size() > 0)		//se la coda del database non � vuota...
		{
			currentJob = this.getPendingJob();		//...pescare dalla coda il prossimo job da servire
			this.setCurrentJob(currentJob);
			this.setNewTime(Clock.time + this.calcCenterDelayTime());		//...aggiornare il calendario calcolando il tempo di servizio
		}
		else		//se la coda del database � vuota...
			this.setNewTime(Parameters.INFINITE);		//...aggiornare il calendario impostando il valore irraggiungibile
	}

	
	/**
	 * Restituisce l'ultimo job uscito dal disco
	 * 
	 * @return lastJob
	 */
	public Job getLastJob() {
		return lastJob;
	}
}
