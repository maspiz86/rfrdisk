package it.pierdiccapizzi.system;

import java.io.Serializable;


/** 
 * Descrive la legge che governa il prng dell'unit� funzionale
 * 
 * @author Pierdicca Luca
 * @author Pizzi Massimo
 */
public enum CenterLawType implements Serializable {
	
	Esponenziale, Iperesponenziale, Kerlangiana
}
