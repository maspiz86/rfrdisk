package it.pierdiccapizzi.system;

import java.io.Serializable;
import it.pierdiccapizzi.eventssequentiator.Clock;
import it.pierdiccapizzi.eventssequentiator.Scheduler;
import it.pierdiccapizzi.generators.Exponential;


/** 
 * Implementa la Wan dell'impianto
 * 
 * @author Pierdicca Luca
 * @author Pizzi Massimo
 */
public class Wan extends Network implements Serializable
{
	private static final long serialVersionUID = 1;
	Exponential wLaw;
	public static int specificCounter = 0;
	
	
	public Wan()
	{	
		super();
		
		specificId = specificCounter;
		specificCounter ++;
		
		typeId = Parameters.WANID;

		type = CenterType.Wan;
		lawType = CenterLawType.Esponenziale;
		lawAverage = Parameters.WANAVG; 
		
		wLaw = new Exponential(seed, lawAverage);
		
		System.out.println(this.getType() + "\t\t\tId: " + typeId + " ; " + specificId);
		System.out.println("Seme " + this.seed + "\t\tIn " + this.attemptNum + " tentativi\n");
	}
	

	@Override
	public double calcCenterDelayTime()
	{
		double d = wLaw.generateNextValue();
		if(Parameters.stampa)System.out.println(this.type+"\t"+d);
		return d;
	}


	@Override
	public void endOfService(Framework impianto)
	{	
		Center nextCenter;
		currentJob = servants[Scheduler.specificId];
		if(this.currentJob.isOutwarding())		//se il job � in andata		
			nextCenter = impianto.getCenter(typeId+1, 0);		//il prossimo centro � il gateway lato server
		else		//se il job � di ritorno
			nextCenter = impianto.getCenter(typeId-1, 0);		//il prossimo centro � il gateway lato client
		
		if(nextCenter.isOccupied)
		{		
			((Gateway) nextCenter).queue.add(this.currentJob);		//mettere il job nella sua coda

		}
		else		//se il prossimo gateway � libero	
		{		
			nextCenter.setCurrentJob(this.currentJob);		//servire il job nel prossimo centro
			nextCenter.setNewTime(Clock.time + nextCenter.calcCenterDelayTime());			//aggiornare il calendario calcolando il tempo di servizio
		}
		
		this.servants[currentJob.getId()] = null;		//liberare il servente 
		this.setNewTime(currentJob.getId(), Parameters.INFINITE);
	}

}