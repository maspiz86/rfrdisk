package it.pierdiccapizzi.vista;

import it.pierdiccapizzi.run.Start;
import it.pierdiccapizzi.system.Parameters;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.border.TitledBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

public class StatConfFrame{

	static JFrame statConfFrame = new JFrame("RFrDisk - Statistical Settings");
	
	Thread statisticT;

	int nC = 0;
	
	JPanel hostPanel;
	JPanel clientsPanel;
	JPanel buttons;
	
	JButton ok;
	JButton back;
	
	JLabel c;
	
	OnlyNumbersTextField clientsN;
	
	
	
	StatConfFrame() {

		statConfFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		statConfFrame.setSize(300, 120);
		statConfFrame.setLocationRelativeTo(null);
		statConfFrame.setResizable(false);
		statConfFrame.setVisible(false);


		hostPanel = new JPanel();
		hostPanel.setLayout(new BoxLayout(hostPanel, BoxLayout.PAGE_AXIS));
		
		clientsPanel = new JPanel();
		
		TitledBorder clientPanelBorder = new TitledBorder("Number of clients");
		
		buttons = new JPanel();
		buttons.setLayout(new FlowLayout(FlowLayout.CENTER,0,0));
		
		clientsN = new OnlyNumbersTextField(4);	
		setClientsNCarLi();
		clientsPanel.setBorder(clientPanelBorder);
		clientsPanel.add(clientsN);

		hostPanel.add(Box.createRigidArea(new Dimension(0,5)));
		hostPanel.add(clientsPanel);
		hostPanel.add(Box.createRigidArea(new Dimension(0,5)));
		
		ok = new JButton("Run");
		setOkAcLi();
		buttons.add(ok);

		back = new JButton("Back");
		setStatCBackAcLi();
		buttons.add(back);

		hostPanel.add(buttons);
		
		statConfFrame.add(hostPanel);
	}
	
	
	public Thread getStatisticT() {
		return statisticT;
	}
	

	void setOkAcLi() {

		ok.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				OutputConsolleFrame.consolleArea.setText("");
				statConfFrame.setVisible(false);
				MainFrame.consolleFrame.consolle.setVisible(true);
				MainFrame.consolleFrame.loading.setIndeterminate(true);
				MainFrame.belongsToStab = false;

				int clients = 0;
				if (!clientsN.getText().isEmpty())
					clients = Integer.parseInt(clientsN.getText());					
				if (clients == 0)
					clients = 12;
				nC = clients;
				
				statisticT = new Thread(new Runnable(){
					public void run() {
						try {
							Start.startTerminalStatistic(nC);
						} catch (ClassNotFoundException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						} catch (URISyntaxException e) {
							e.printStackTrace();
						}
					}
				});
				
				statisticT.start();
				
			}
		});
	}
	
	void setClientsNCarLi() {

		clientsN.addCaretListener(new CaretListener(){

			public void caretUpdate(CaretEvent e) {

				int clients = 0;
				if (!clientsN.getText().isEmpty())
					clients = Integer.parseInt(clientsN.getText());					
				if (clients == 0)
					clients = 12;
				nC = clients;

				if(nC>100){
					clientsN.setBackground(Color.RED);
					ok.setEnabled(false);
				}
				else{
					clientsN.setBackground(Color.WHITE);
					ok.setEnabled(true);
				}

			}

		});
	}


	void setStatCBackAcLi() {

		back.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
			
				statConfFrame.setVisible(false);
				MainFrame.mainFrame.setVisible(true);
			}
		});
	}
}
