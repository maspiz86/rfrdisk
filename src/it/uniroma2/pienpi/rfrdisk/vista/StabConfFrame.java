package it.pierdiccapizzi.vista;

import it.pierdiccapizzi.run.Start;
import it.pierdiccapizzi.system.Framework;
import it.pierdiccapizzi.system.Parameters;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ChangeListener;

public class StabConfFrame {

	public static JFrame stabConfFrame = new JFrame("RFrDisk - Stabilization Settings");

	Thread stabilizationT; 

	int nP, nO, nC;

	JPanel hostPanel, clientsPanel, observationPanel, pilotrunPanel, buttons;

	JCheckBox choice;

	TitledBorder clientPanelBorder, observationPanelBorder, pilotrunPanelBorder;

	static OnlyNumbersTextField clientsN;
	static OnlyNumbersTextField observationsN;
	static OnlyNumbersTextField pilotsN;

	JButton ok;
	JButton back;



	StabConfFrame() {

		stabConfFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		stabConfFrame.setSize(380, 242);
		stabConfFrame.setLocationRelativeTo(null);
		stabConfFrame.setResizable(false);
		stabConfFrame.setVisible(false);

		hostPanel = new JPanel();
		clientsPanel = new JPanel();
		observationPanel = new JPanel();
		pilotrunPanel = new JPanel();
		buttons = new JPanel();

		choice = new JCheckBox("Always reach stability");
		setChoiceAcLi();

		observationPanel.setLayout(new FlowLayout(FlowLayout.LEFT,0,0));

		buttons.setLayout(new FlowLayout(FlowLayout.CENTER,0,0));

		hostPanel.setLayout(new BoxLayout(hostPanel, BoxLayout.PAGE_AXIS));

		clientPanelBorder = new TitledBorder("Number of clients");
		observationPanelBorder = new TitledBorder("Max number of observations");
		pilotrunPanelBorder = new TitledBorder("Number of pilot runs");

		clientsN = new OnlyNumbersTextField(7);	
		clientsPanel.setBorder(clientPanelBorder);
		setClientsNCarLiPlus();
		setClientsNCarLi();
		clientsPanel.add(clientsN);

		observationsN = new OnlyNumbersTextField(7);

		observationPanel.setBorder(observationPanelBorder);
		observationPanel.add(Box.createRigidArea(new Dimension(150,35)));
		observationPanel.add(observationsN);
		observationPanel.add(Box.createRigidArea(new Dimension(20,35)));
		observationPanel.add(choice);
		setObservationsNCarLi();

		pilotsN = new OnlyNumbersTextField(7);
		pilotrunPanel.setBorder(pilotrunPanelBorder);
		pilotrunPanel.add(pilotsN);
		setPilotRunNCarLi();

		hostPanel.add(Box.createRigidArea(new Dimension(0,5)));
		hostPanel.add(clientsPanel);
		hostPanel.add(Box.createRigidArea(new Dimension(0,5)));
		hostPanel.add(observationPanel);
		hostPanel.add(Box.createRigidArea(new Dimension(0,5)));
		hostPanel.add(pilotrunPanel);
		hostPanel.add(Box.createRigidArea(new Dimension(0,5)));

		ok = new JButton("Run");
		setOkAcLi();
		buttons.add(ok);

		back = new JButton("Back");
		setStabCBackAcLi();
		buttons.add(back);

		hostPanel.add(buttons);

		stabConfFrame.add(hostPanel);

	}


	public Thread getStabilizationT() {
		return stabilizationT;
	}


	void setClientsNCarLiPlus(){
		clientsN.addCaretListener(new CaretListener(){
			public void caretUpdate(CaretEvent e) {

				int clients = 0;
				if (!clientsN.getText().isEmpty())
					clients = Integer.parseInt(clientsN.getText());					
				if (clients == 0)
					clients = 12;
				nC = clients;

				int observations = 0;
				if (!observationsN.getText().isEmpty())
					observations = Integer.parseInt(observationsN.getText());	
				if (observations == 0)
					observations = 61;
				nO = observations;

				int checkObs = Parameters.exactObservations[nC-1];

				if(nO < checkObs)
					observationsN.setBackground(Color.ORANGE);
				else
					observationsN.setBackground(Color.WHITE);
			}

		});
	}


	void setClientsNCarLi(){
		clientsN.addCaretListener(new CaretListener(){
			public void caretUpdate(CaretEvent e) {

				int clients = 0;
				if (!clientsN.getText().isEmpty())
					clients = Integer.parseInt(clientsN.getText());					
				if (clients == 0)
					clients = 12;
				nC = clients;

				if(nC <= Parameters.exactObservations.length) {

					clientsN.setBackground(Color.WHITE);
					ok.setEnabled(true);
					observationsN.setEditable(true);
					choice.setEnabled(true);
					pilotsN.setEnabled(true);

					if(choice.isSelected())
						observationsN.setText(String.valueOf(Parameters.exactObservations[nC-1]));
				}
				else if(Parameters.exactObservations.length < nC  &&  nC <= 329) {	

					clientsN.setBackground(Color.LIGHT_GRAY);
					ok.setEnabled(true);
					choice.setSelected(false);
					choice.setEnabled(false);
					observationsN.setEnabled(true);
					observationsN.setBackground(Color.LIGHT_GRAY);
					pilotsN.setEnabled(true);

				}
				else {

					clientsN.setBackground(Color.RED);
					ok.setEnabled(false);
					choice.setSelected(false);
					choice.setEnabled(false);
					observationsN.setEnabled(false);
					pilotsN.setEnabled(false);
				}
			}

		});
	}


	void setObservationsNCarLi() {

		observationsN.addCaretListener(new CaretListener(){

			public void caretUpdate(CaretEvent e) {

				int observations = 0;
				if (!observationsN.getText().isEmpty())
					observations = Integer.parseInt(observationsN.getText());	
				if (observations == 0)
					observations = 61;
				nO = observations;

				int checkObs = Parameters.exactObservations[nC-1];

				if(nO < checkObs)
					observationsN.setBackground(Color.ORANGE);
				else
					observationsN.setBackground(Color.WHITE);

			}

		});
	}


	void setPilotRunNCarLi() {

		pilotsN.addCaretListener(new CaretListener(){

			public void caretUpdate(CaretEvent e) {

				int pilots = 0;
				if (!pilotsN.getText().isEmpty())
					pilots = Integer.parseInt(pilotsN.getText());
				if(pilots == 0)
					pilots = 31;
				nP = pilots;

				if(nP <= 30)
					pilotsN.setBackground(Color.ORANGE);
				else
					pilotsN.setBackground(Color.WHITE);

			}

		});
	}


	void setChoiceAcLi(){
		choice.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {

				int clients = 0;
				if (!clientsN.getText().isEmpty())
					clients = Integer.parseInt(clientsN.getText());					
				if (clients == 0)
					clients = 12;
				nC = clients;

				if(choice.isSelected()){

					if(nC > 0 && nC<= Parameters.exactObservations.length ){
						observationsN.setText(String.valueOf(Parameters.exactObservations[nC-1]));
						observationsN.setEnabled(false);
					}
					else{
						choice.setSelected(false);
						choice.setEnabled(false);
						observationsN.setEnabled(true);
					}
				}
				else{
					observationsN.setEnabled(true);
					observationsN.setText("");
				}

			}
		});
	}


	void setOkAcLi() {

		ok.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				int clients = 0;
				if (!clientsN.getText().isEmpty())
					clients = Integer.parseInt(clientsN.getText());					
				if (clients == 0)
					clients = 12;
				nC = clients;

				int observations = 0;
				if (!observationsN.getText().isEmpty())
					observations = Integer.parseInt(observationsN.getText());	
				if (observations == 0)
					observations = 61;
				nO = observations;

				int pilots = 0;
				if (!pilotsN.getText().isEmpty())
					pilots = Integer.parseInt(pilotsN.getText());
				if(pilots == 0)
					pilots = 31;
				nP = pilots;

				OutputConsolleFrame.consolleArea.setText("");
				stabConfFrame.setVisible(false);
				MainFrame.consolleFrame.consolle.setVisible(true);
				MainFrame.consolleFrame.loading.setIndeterminate(true);
				MainFrame.belongsToStab = true;

				stabilizationT = new Thread(new Runnable(){
					public void run() {
						try {
							Start.generateSteadyState(nC, nP, nO);
						} catch (ClassNotFoundException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				});

				stabilizationT.start();
			}
		});
	}


	void setStabCBackAcLi() {

		back.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				stabConfFrame.setVisible(false);
				MainFrame.mainFrame.setVisible(true);

			}
		});
	}
	
	
	static String getC() {
		return clientsN.getText();	
	}


	static String getO() {

		return observationsN.getText();
	}


	static String getP() {
		return pilotsN.getText();
	}
}


