# **RFRDISK SIMULATION TOOL** #

Questa è una piccola guida utente, inerente all'utilizzo del programma tramite sue interfacce grafiche.
Per saperne di più sull'implementazione, leggere il report presente nella cartella utils.

Avviando il programma viene visualizzato il frame principale che mostra l'EQN dell'impianto e da cui è possibile scegliere se effettuare un run di stabilizzazione oppure un run statistico.

## STABILIZATION ##
Nella finestra di configurazione dei parametri per la stabilizzazione è possibile impostare:

* Il numero dei client presenti nell'impianto da simulare [1, 329];
* Il numero massimo di osservazioni da effettuare per stabilizzare;
* Il numero di run pilota da effettuare.

Può essere simulato un impianto fino a un massimo di 329 client garantendo che ogni unità funzionale possieda un seme differente da ogni altro.
Il numero di osservazioni da effettuare è totalmente facoltativo, ma ogni impianto ha un numero minimo di osservazioni a cui si deve arrivare per poter essere garantita la stabilità; è possibile per impianti da 1 a 100 client utilizzare l'apposita check box per determinare automaticamente il numero minimo di osservazioni a cui arrivare affinchè sia garantita l'eliminazione della polarizzazione iniziale.
Il numero dei run pilota è anch'esso totalmente facoltativo, ma per una buona stabilizzazione deve essere superiore a 29.

## Run di stabilizzazione ##
L'output descrive:
 
* La creazione;
* La stabilizzazione
* Il run statistico.

Durante la creazione, per ogni centro vengono esplicitati il numero identificativo all'interno dell'impianto e il seme acquisito (assunto random tra tutti i primi compresi tra 1 e 5000).
La stabilizzazione fornisce principalmente la media stimata tramite Gordon.
Il run statistico presenta la media campionaria, la varianza campionaria e i limiti della media teorica.

## STATISTICAL ##
In questa finestra di configurazione il parametro è unicamente quello che determina il numero dei cliient presenti nell'impianto da simulare.
E' possibile effettuare un run statistico su ogni impianto che abbia fino a 100 client.

## Run Statistico ##
L'output in questo caso è strettamente relativo al run statistico, mostrando la media campionaria, la varianza campionaria e i limiti della media teorica.

## LIGHT VERSION ##
Abbiamo creato una versione più compatta nella sua GUI che però presenta alcune feature aggiuntive qui descritte:

* Possibilità di acquisire semi ordinati;
* Salvataggio di un log contenente i risultati delle operazioni effettuate.

Se i semi vengono acquisiti in ordine, ogni volta che viene creato un impianto con esattamente C client, questi acquisiscono sempre gli stessi semi; ciò permette sia di osservare il comportamento di uno stesso impianto al variare di ogni singolo altro parametro, sia di garantire la riproducibilità dell'esperimento.

## AUTHORS ##
* Luca Pierdicca
* Massimo Pizzi