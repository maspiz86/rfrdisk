package it.pierdiccapizzi.run;

import java.io.IOException;
import java.net.URISyntaxException;

import it.pierdiccapizzi.eventssequentiator.Scheduler;
import it.pierdiccapizzi.system.Parameters;
import it.pierdiccapizzi.vista.MainFrame;
import it.pierdiccapizzi.vista.OutputConsolleFrame;


/**
 * La classe che avvia il tool
 * 
 * @author Pierdicca Luca
 * @author Pizzi Massimo
 */
public class Start {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		 
		Parameters.setExactObservations();
		MainFrame controlPanel = new MainFrame();

	}


	/**
	 * Stabilizza l'impianto sul tempo di risposta visto ai terminali e ne effettua la stima
	 * 
	 * @param numeroClient				il numero di client presenti nell'impianto
	 * @param numeroRunPilota			se 0 verranno effettuati 60 run pilota
	 * @param numeroOsservazioni		se 0 verranno effettuate esattamente le osservazioni necessarie alla stabilizzazioni
	 * 
	 * @throws IOException
	 * @throws ClassNotFoundException 
	 */
	public static void generateSteadyState(int numeroClient, int numeroRunPilota, int numeroOsservazioni) throws IOException, ClassNotFoundException {

		Scheduler stabSched = new Scheduler();

		stabSched.stabilizeOnTerminalsResponseTime(numeroClient, numeroRunPilota, numeroOsservazioni);

		stabSched.runTermStat();
		
		OutputConsolleFrame.loading.setIndeterminate(false);
	}


	/**
	 * Deserializza un impianto stabilizzato e effettua la stima del tempo di risposta visto ai terminali
	 * 
	 * @param numeroClient			il numero dei client presenti nell'impianto che si vuole stimare
	 * 
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @throws URISyntaxException 
	 */
	public static void startTerminalStatistic(int numeroClient) throws ClassNotFoundException, IOException, URISyntaxException {

		Scheduler statSched = new Scheduler();

		statSched.estimateGlobalAverageResponseTimeOnTerminals(numeroClient);
		
		OutputConsolleFrame.loading.setIndeterminate(false);
	}


}
