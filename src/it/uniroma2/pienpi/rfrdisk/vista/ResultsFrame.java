package it.pierdiccapizzi.vista;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

public class ResultsFrame {

	JFrame results;

	double gA;

	JLabel trace;
	JPanel show;
	String c, o, p;
	TitledBorder gAB;
	
	public ResultsFrame(double media) {

		gA = media;

		results = new JFrame();
		results = new JFrame("RFrDisk - Elaborator Average");                                        // il titolo del frame
		results.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);           // l'azione del pulsante di uscita
		results.setSize(300, 100);                                         // dimensione iniziale
		results.setLocationRelativeTo(null);                              // posizione centrale
		results.setResizable(false);
		results.setVisible(true);                                     // mostra il frame

		
		c = StabConfFrame.getC();
		if (c.equals(""))
			c = "12";
		
		o = StabConfFrame.getO();
		if (o.equals(""))
			o = "61";
		
		p = StabConfFrame.getP();
		if (p.equals(""))
			p = "30";
		
		gAB = new TitledBorder(c + " clients, " + o + " observations, " + p + " pilot runs");
		
		show = new JPanel();
		show.setBorder(gAB);
		
		trace = new JLabel("" + gA);
		trace.setLayout(new FlowLayout(FlowLayout.CENTER));
		show.add(trace);

		results.add(show);
	}
}
