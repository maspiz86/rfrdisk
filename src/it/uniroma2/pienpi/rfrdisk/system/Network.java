package it.pierdiccapizzi.system;

import java.io.Serializable;
import it.pierdiccapizzi.eventssequentiator.Job;
import it.pierdiccapizzi.eventssequentiator.Scheduler;



/** 
 * Rappresenta una generica rete dell'impianto
 * 
 * @author Pierdicca Luca
 * @author Pizzi Massimo
 */
public abstract class Network extends Center implements Serializable {

	private static final long serialVersionUID = 1;
	Job[] servants;
		
	
	
	public Network(){
		super();
		servants = new Job[Parameters.clientNum];
	}
	
	
	/**
	 * Imposta il numero dei serventi di cui la rete � costituita
	 * 
	 * @param numServenti
	 */
	public void setServantsNum(int numServenti){
		servants = new Job[numServenti];
	}
	
	
	/**
	 * Restituisce un job nella rete
	 * 
	 * @param id		Il servente dal quale si vuole ricavare il job
	 * @return
	 */
	public Job getCurrentJob(int id){
		return servants[id];
	}
	
	
	/**
	 * Assegna un job a uno dei serventi della rete
	 * 
	 * @param jobCorrente		Il job da assegnare
	 */
	public void setCurrentJob(Job jobCorrente) {
		
		servants[jobCorrente.getId()] = jobCorrente;
	}
	
	
	/**
	 * Svuota la rete da ogni job
	 */
	public void clearServants() {
		
		for(int i=0;i<this.servants.length;i++)
			this.servants[i] = null;
	}
	
	
	@Override
	public abstract double calcCenterDelayTime();


	@Override
	public abstract void endOfService(Framework impianto);
	
	
	/**
	 * Imposta il tempo di servizio
	 * 
	 * @param servente		Il servente a cui si fa riferimento
	 * @param tempo			Il tempo di servizio per il job in quel servente
	 */
	public void setNewTime(int servente, double tempo) {
		
		Scheduler.currentCalendar.centerType.get(this.typeId).set(servente, tempo);
	}
}
