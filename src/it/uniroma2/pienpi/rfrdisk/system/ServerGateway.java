package it.pierdiccapizzi.system;

import java.io.Serializable;
import it.pierdiccapizzi.eventssequentiator.Clock;
import it.pierdiccapizzi.eventssequentiator.Job;
import it.pierdiccapizzi.generators.KErlang;



/** 
 * Implementa un gateway lato server
 * 
 * @author Pierdicca Luca
 * @author Pizzi Massimo
 */
public class ServerGateway extends Gateway implements Queuable, Serializable {
	
	private static final long serialVersionUID = 1;
	double parameterKerl;
	KErlang gWSeLaw;
	public static int specificCounter = 0;
	
	
	
	public ServerGateway() {
		
		super();

		typeId = Parameters.SGWID;
		specificId = specificCounter;
		specificCounter++;

		type = CenterType.GatewaySE;
		lawType = CenterLawType.Kerlangiana;
		lawAverage = Parameters.SGWAVG;
		
		parameterKerl = Parameters.SGWPAR;
		gWSeLaw = new KErlang(seed, lawAverage, parameterKerl);
		
		System.out.println(this.getType() + "\t\tId: " + typeId + " ; " + specificId);
		System.out.println("Seme " + this.seed + "\t\tIn " + this.attemptNum + " tentativi\n");
	}
	
	
	@Override
	public Job getPendingJob() {		//Disciplina LIFO
		return this.queue.pollLast();
	}

	
	@Override
	public double calcCenterDelayTime()
	{
		double d = gWSeLaw.generateNextValue();
		if(Parameters.stampa)System.out.println(this.type+"\t"+d);
		return d;
	}

	
	@Override
	public void endOfService(Framework impianto)
	{	
		Center nextCenter;
		
		if(currentJob.isOutwarding()) {							//se il job � in andata...
			
			nextCenter = impianto.getCenter(typeId+1, 0);			//...il prossimo centro sar� la server lan
			((ServerLan)nextCenter).lanSeLaw.setAverage(Parameters.SLANAVGClass1);
		}
		else													//se il job � di ritorno...
			nextCenter = impianto.getCenter(typeId-1, 0);			//...il prossimo centro sar� la wan

		nextCenter.setCurrentJob(this.currentJob);				//inserire il job in un servente della rete in qestione
		((Network) nextCenter).setNewTime(currentJob.getId(), Clock.time + nextCenter.calcCenterDelayTime());		//aggiornare il calendario calcolando il tempo di servizio	
		this.setCurrentJob(null);
		
		if(this.queue.size() > 0) {					//se la coda del gateway non � vuota
		
			currentJob = this.getPendingJob();			//pescare dalla coda il prossimo job da servire
			this.setCurrentJob(currentJob);
			this.setNewTime(Clock.time + this.calcCenterDelayTime());		//aggiornare il calendario calcolando il tempo di servizio
		}
		else
			this.setNewTime(Parameters.INFINITE);		//aggiornare il calendario impostando il valore irraggiungibile
	}
}