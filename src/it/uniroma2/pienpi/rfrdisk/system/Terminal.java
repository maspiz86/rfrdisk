package it.pierdiccapizzi.system;

import java.io.Serializable;

import it.pierdiccapizzi.eventssequentiator.Clock;
import it.pierdiccapizzi.eventssequentiator.Job;
import it.pierdiccapizzi.generators.HyperExponential;


/** 
 * Implementa uno dei terminali lato client
 * 
 * @author Pierdicca Luca
 * @author Pizzi Massimo
 */
public class Terminal  extends Center implements Serializable
{
	
	private static final long serialVersionUID = 1;
	double parameterIperExp;
	HyperExponential tLaw;
	public static Integer specificCounter = 0;
			
	
	public Terminal()
	{
		super();

		specificId = specificCounter;
		specificCounter++;
		
		typeId = Parameters.TERMID;
		
		type = CenterType.Terminale;
		lawType = CenterLawType.Iperesponenziale;
		lawAverage = Parameters.TERMAVG;
		
		parameterIperExp = Parameters.TERMPAR;
		tLaw = new HyperExponential(seed, lawAverage, parameterIperExp);
		
		System.out.println(this.getType() + "\t\tId: " + typeId + " ; " + specificId);
		System.out.println("Seme " + this.seed + "\t\tIn " + this.attemptNum + " tentativi\n");
	}
	
	
	public void jobApproach()		//avvia il tour per un nuovo job
	{
		currentJob = new Job();
		currentJob.setId(this.specificId);
		this.setNewTime(Clock.time + this.calcCenterDelayTime());
	}
	


	@Override
	public double calcCenterDelayTime()
	{
		double d = tLaw.generateNextValue();
		if(Parameters.stampa)System.out.println(this.type+"\t"+d);
		return d;
		
	}


	@Override
	public void endOfService(Framework impianto)
	{
		Center nextCenter = impianto.getCenter(typeId+1, specificId);
		nextCenter.setCurrentJob(this.currentJob);
		nextCenter.setNewTime(Clock.time + nextCenter.calcCenterDelayTime());
		this.setNewTime(Parameters.INFINITE);
		this.setCurrentJob(null);
	}
	
	public void resetSpecificCounter(){
		specificCounter = 0;
	}
	
}
