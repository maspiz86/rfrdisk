package it.pierdiccapizzi.system;

import it.pierdiccapizzi.eventssequentiator.Job;


/** 
 * Rappresenta una generica coda nell'impianto
 * 
 * @author Pierdicca Luca
 * @author Pizzi Massimo
 */
public interface Queuable {
		
	public Job getPendingJob();
}
