package it.pierdiccapizzi.system;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;

import it.pierdiccapizzi.observationselaborator.Elaborator;


/** 
 * Implementa l'impianto in se
 * 
 * @author Pierdicca Luca
 * @author Pizzi Massimo
 */
 /*
  * +--------------------------STRUTTURA IMPIANTO-----------------------+
  * |	 													            |
  * |	 T  [ 0 ]-->|0|-|1|-|2|-|3|-|4|-|5|-|6|-|7|-|8|-|9|-|10|-|11|	|													
  * |	 cP [ 1 ]-->|0|-|1|-|2|-|3|-|4|-|5|-|6|-|7|-|8|-|9|-|10|-|11|	|													
  * |	 cL [ 2 ]-->|0|-servants->([0]~[11])                            |
  * |	 cG [ 3 ]-->|0|													|
  * |	 W  [ 4 ]-->|0|-servants->([0]~[11])                            |
  * |	 sG [ 5 ]-->|0|													|
  * |	 sL [ 6 ]-->|0|-servants->([0]~[11])							|
  * |	 sP [ 7 ]-->|0|-|1|-|2|											|
  * |	 D  [ 8 ]-->|0|-|1|-|2|											|
  * |																	|
  * |      [] Array														|
  * |      | | ArrayList												|
  * +-------------------------------------------------------------------+
  */
public class Framework implements Serializable {

	private static final long serialVersionUID = 1;

	transient Elaborator estimator;

	public static int stabRttObservationsAchieved;
	public static int statRttObservationsAchieved;
	public static int statDiskObservationsAchieved;

	int nZero;
	int numHostClient;

	ArrayList[] impianto = new ArrayList[Parameters.SYSTEMSTEPS]; 

	//ArrayList di tipo Center che popolano l'Array impianto
	ArrayList<Terminal> term = new ArrayList<Terminal>();
	ArrayList<ClientProcessor> cCpu = new ArrayList<ClientProcessor>();
	ArrayList<ClientLan> cLan = new ArrayList<ClientLan>();
	ArrayList<ClientGateway> cGw = new ArrayList<ClientGateway>();
	ArrayList<Wan> wan = new ArrayList<Wan>();
	ArrayList<ServerGateway> sGw = new ArrayList<ServerGateway>();
	ArrayList<ServerLan> sLan = new ArrayList<ServerLan>();
	ArrayList<ServerProcessor> sCpu = new ArrayList<ServerProcessor>();
	ArrayList<Disk> sDisk = new ArrayList<Disk>();



	public Framework()
	{	 
		System.out.println("\n##### COSTRUZIONE DELL'IMPIANTO #####\n");
		
		for(int k=0 ; k<Parameters.clientNum ; k++) term.add(new Terminal());
		for(int k=0 ; k<Parameters.clientNum ; k++) cCpu.add(new ClientProcessor());	 
		for(int k=0 ; k<Parameters.CLANNUM ; k++) cLan.add(new ClientLan());
		for(int k=0 ; k<Parameters.CGWNUM ; k++) cGw.add(new ClientGateway());
		for(int k=0 ; k<Parameters.WANNUM ; k++) wan.add(new Wan());
		for(int k=0 ; k<Parameters.SGWNUM ; k++) sGw.add(new ServerGateway());
		for(int k=0 ; k<Parameters.SLANNUM ; k++) sLan.add(new ServerLan());
		for(int k=0 ; k<Parameters.serverNum ; k++) sCpu.add(new ServerProcessor());
		for(int k=0 ; k<Parameters.serverNum ; k++) sDisk.add(new Disk(this));

		impianto[0] = term;
		impianto[1] = cCpu;
		impianto[2] = cLan;
		impianto[3] = cGw;
		impianto[4] = wan;
		impianto[5] = sGw;
		impianto[6] = sLan;
		impianto[7] = sCpu;
		impianto[8] = sDisk;

		estimator = new Elaborator();
		resetCreationCounter();
	}


	/**
	 * Aggiunge all'impianto un nuovo client e scala le reti per ospitarne il traffico
	 */
	public void addISCenter(){

		Parameters.clientNum = Parameters.clientNum + 1;

		term.add(new Terminal());
		cCpu.add(new ClientProcessor());

		int currServNum = cLan.get(0).servants.length;
		cLan.get(0).setServantsNum(currServNum + 1);
		wan.get(0).setServantsNum(currServNum + 1);
		sLan.get(0).setServantsNum(currServNum + 1);
	}


	/**
	 * Restituisce il numero di osservazioni effettuate per stabilizzare l'impianto
	 * 
	 * @return nZero
	 */
	public int getNzero(){
		return nZero;
	}


	/**
	 * Imposta il numero delle osservazioni necessarie a stabilizzare l'impianto
	 * 
	 * @param nZero
	 */
	public void setNzero(int nZero){
		this.nZero = nZero;
	}


	/**
	 * Restituisce l'oggetto che si occuper� di effettuare le stime su media e varianza
	 * 
	 * @return estimator
	 */
	public Elaborator getEstimator(){
		return estimator;
	}


	/**
	 * Imposta un oggetto in grado di effettuare le stime
	 * @param stimatore
	 */
	public void setEstimator(Elaborator stimatore) {
		estimator = stimatore;
	}


	/**
	 * Restituisce un centro dell'impianto
	 * 
	 * @param tipoDiCentro			il numero che identifica la tipologia del centro
	 * @param centroSpecifico		il numero che iedntifica il centro tra tutti quelli del suo stesso tipo	
	 * @return 
	 */
	public Center getCenter(int tipoDiCentro, int centroSpecifico){
		if(tipoDiCentro == Parameters.CLANID || tipoDiCentro == Parameters.WANID || tipoDiCentro == Parameters.SLANID)
			return (Center) impianto[tipoDiCentro].get(0);
		else
			return (Center) impianto[tipoDiCentro].get(centroSpecifico);
	}


	/**
	 * Restituisce l'insieme dei terminali presenti nell'impianto
	 * 
	 * @return
	 */
	public ArrayList<Terminal> getTerminals(){
		return impianto[Parameters.TERMID];
	}


	/**
	 * Riporta l'impianto alle condizioni inziali: tutti i centri liberi e tutte le code libere
	 */
	public void reset(){
		for(int k=0 ; k<(Parameters.clientNum) ; k++) {
			term.get(k).setCurrentJob(null);
		}

		for(int k=0 ; k<Parameters.clientNum ; k++){
			cCpu.get(k).setCurrentJob(null);
		}

		((Network) cLan.get(0)).clearServants();

		for(int k=0 ; k<Parameters.CGWNUM ; k++){
			cGw.get(k).setCurrentJob(null);
			((Gateway)cGw.get(k)).queue.clear();
		}

		((Network) wan.get(0)).clearServants();

		for(int k=0 ; k<Parameters.SGWNUM ; k++) {
			sGw.get(k).setCurrentJob(null);
			((Gateway)sGw.get(k)).queue.clear();
		}

		((Network) sLan.get(0)).clearServants();

		for(int k=0 ; k<Parameters.SCPUNUM ; k++) {
			sCpu.get(k).setCurrentJob(null);
			sCpu.get(k).queue.clear();
		}

		for(int k=0 ; k<Parameters.SDISKNUM ; k++) {
			sDisk.get(k).setCurrentJob(null);
			sDisk.get(k).queue.clear();
		}
		
		stabRttObservationsAchieved = 0;
	}

	public static void resetCreationCounter(){
		
		Terminal.specificCounter = 0;
		ClientProcessor.specificCounter = 0;
		ClientLan.specificCounter = 0;
		ClientGateway.specificCounter = 0;
		Wan.specificCounter = 0;
		ServerGateway.specificCounter = 0;
		ServerLan.specificCounter = 0;
		ServerProcessor.specificCounter = 0;
		Disk.specificCounter = 0;
		
	}
}