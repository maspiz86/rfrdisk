package it.pierdiccapizzi.system;

import java.io.Serializable;
import java.util.LinkedList;
import it.pierdiccapizzi.eventssequentiator.Clock;
import it.pierdiccapizzi.eventssequentiator.Job;
import it.pierdiccapizzi.generators.Exponential;
import it.pierdiccapizzi.generators.RandomSequence;
import it.pierdiccapizzi.generators.Uniform;


/** 
 * Implementa un Pc lato server
 * 
 * @author Pierdicca Luca
 * @author Pizzi Massimo
 */
public class ServerProcessor extends Center implements Queuable, Serializable
{
	private static final long serialVersionUID = 1;
	Exponential pLaw;
	public static int specificCounter = 0;
	LinkedList<Job> queue;
	RandomSequence linConProbOut;
	Uniform unif;
	double currentProbOut;
	Job lastJob;



	ServerProcessor() {

		super();

		linConProbOut = new RandomSequence(this.seed);
		unif = new Uniform(this.seed, linConProbOut);

		queue = new LinkedList<Job>();

		typeId = Parameters.SCPUID;
		specificId = specificCounter;
		specificCounter++;

		type = CenterType.ProcessoreSE;
		lawType = CenterLawType.Esponenziale;
		lawAverage = Parameters.SCPUAVG;

		pLaw = new Exponential(seed, lawAverage);

		System.out.println(this.getType() + "\t\tId: " + typeId + " ; " + specificId);
		System.out.println("Seme " + this.seed + "\t\tIn " + this.attemptNum + " tentativi\n");
	}

	@Override
	public Job getPendingJob() {		//Disciplina LIFO
		return this.queue.pollLast();
	}


	@Override
	public double calcCenterDelayTime() {

		double d = pLaw.generateNextValue();
		if(Parameters.stampa)System.out.println(this.type+"\t"+d);
		return d;
	}


	public Job getLastJob() {
		return lastJob;
	}


	@Override
	public void endOfService(Framework impianto) {

		Center nextCenter;
		double diskDelayTime;

		currentProbOut = unif.generateNextValue();

		if(currentJob.isOutwarding() && currentProbOut > Parameters.PROBOUT) {		//se il job � in andata e non ha ancora fatto i 50 accessi al database...

			nextCenter = impianto.getCenter(typeId+1, specificId); 				//...il prossimo centro sar� il database

			if(nextCenter.isOccupied)									//se il database � occupato...
				((Disk) nextCenter).queue.add(this.currentJob);				//mandare il job in coda
			else {														//se il database � libero...

				nextCenter.setCurrentJob(this.currentJob);					//...servir� direttamente questo job
				nextCenter.setNewTime(Clock.time + nextCenter.calcCenterDelayTime());		//...aggiornare il calendario calcolando il tempo di servizio
			}			
		}
		else {												//se il job ha fatto i 50 accessi...

			currentJob.setOutwarding(false);					//...il job inizier� il ritorno

			nextCenter = impianto.getCenter(typeId-1, 0);		//...il prossimo centro sar� la server lan

			nextCenter.setCurrentJob(this.currentJob);		//inserire il job in un servente
			((ServerLan)nextCenter).lanSeLaw.setAverage(Parameters.SLANAVGClass2);
			((Network) nextCenter).setNewTime(currentJob.getId(), Clock.time + nextCenter.calcCenterDelayTime());		//aggiornare il calendario calcolando il tempo di servizio	
		}

		lastJob = currentJob;
		this.setCurrentJob(null);

		if(this.queue.size() > 0) {			//se la coda del server processor non � vuota

			currentJob = this.getPendingJob();		//pescare dalla coda il prossimo job da servire
			this.setCurrentJob(currentJob);
			this.setNewTime(Clock.time + this.calcCenterDelayTime());		//aggiornare il calendario calcolando il tempo di servizio
		}
		else
			this.setNewTime(Parameters.INFINITE);		//aggiornare il calendario impostando il valore irraggiungibile
	}	
}