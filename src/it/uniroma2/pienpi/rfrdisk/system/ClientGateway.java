package it.pierdiccapizzi.system;


import java.io.Serializable;

import it.pierdiccapizzi.eventssequentiator.Clock;
import it.pierdiccapizzi.eventssequentiator.Job;
import it.pierdiccapizzi.generators.HyperExponential;


/** 
 * Implementa un gateway lato client
 * 
 * @author Pierdicca Luca
 * @author Pizzi Massimo
 */
public class ClientGateway extends Gateway implements Queuable, Serializable {
	
	private static final long serialVersionUID = 1;
	double parameterIperExp;
	HyperExponential gWClLaw;
	public static int specificCounter = 0;
	
	
	
	public ClientGateway() {	
		
		super();
		
		specificId = specificCounter;
		specificCounter++;
		
		typeId = Parameters.CGWID;
		
		type = CenterType.GatewayCL;
		lawType = CenterLawType.Iperesponenziale;
		lawAverage = Parameters.CGWAVG;
		parameterIperExp = Parameters.CGWPAR;
		
		gWClLaw = new HyperExponential(seed, lawAverage, parameterIperExp);	
		
		System.out.println(this.getType() + "\t\tId: " + typeId + " ; " + specificId);
		System.out.println("Seme " + this.seed + "\t\tIn " + this.attemptNum + " tentativi\n");
	}
	
	
	@Override
	public Job getPendingJob() {		//Disciplina LIFO
		return this.queue.pollLast();
	}

	
	@Override
	public double calcCenterDelayTime()
	{
		double d = gWClLaw.generateNextValue();
		if(Parameters.stampa)System.out.println(this.type+"\t"+d);
		return d;
	}

	
	@Override
	public void endOfService(Framework impianto)
	{	
		Center nextCenter;
		
		if(currentJob.isOutwarding())		//se il job � in andata...
		{	
			nextCenter = impianto.getCenter(typeId+1, 0);		//...il prossimo centro sar� la wan
		}
		else 		//se il job � di ritorno...
		{	
			nextCenter = impianto.getCenter(typeId-1, 0);		//...il prossimo centro sar� la client lan
			((ClientLan)nextCenter).lanClLaw.setAverage(Parameters.CLANAVGClass2);

		}
		
		nextCenter.setCurrentJob(this.currentJob);		//inserire il job in un servente della rete in qestione
		((Network) nextCenter).setNewTime(currentJob.getId(), Clock.time + nextCenter.calcCenterDelayTime());		//aggiornare il calendario calcolando il tempo di servizio	
		this.setCurrentJob(null);
		
		if(this.queue.size() > 0)		//se la coda non � vuota
		{
			currentJob = this.getPendingJob();		//pescare dalla coda il prossimo job da servire
			this.setCurrentJob(currentJob);
			this.setNewTime(Clock.time + this.calcCenterDelayTime());		//aggiornare il calendario calcolando il tempo di servizio
		}
		else
			this.setNewTime(Parameters.INFINITE);		//aggiornare il calendario impostando il valore irraggiungibile
	}
}