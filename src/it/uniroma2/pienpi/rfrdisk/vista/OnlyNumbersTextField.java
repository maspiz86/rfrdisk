package it.pierdiccapizzi.vista;

import java.awt.FlowLayout;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

public class OnlyNumbersTextField extends JTextField
{
		/*
	 * Costruttore con unico parametro il limite della textfield
	 */
	public OnlyNumbersTextField(int cols)
	{	
		super(cols);
		setDocument(new TextLimitDocument());

	}

	/***************************************************************************
	 * Classe interna per la gestione dell'input

	 **************************************************************************/
	private class TextLimitDocument extends PlainDocument
	{
		/*
		 * Funzione per l'inserimento della stringa nella textfield
		 */
		public void insertString(int offset, String str, AttributeSet attr) throws BadLocationException {

			boolean invalid = false;

			if (str == null)
				return;

			if (!str.matches("[0-9]*"))
				return;

			super.insertString(offset, str, attr);
		}
	}
}