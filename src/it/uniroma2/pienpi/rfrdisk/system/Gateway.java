package it.pierdiccapizzi.system;

import java.io.Serializable;
import java.util.LinkedList;

import it.pierdiccapizzi.eventssequentiator.Job;


/** 
 * Rappresenta un generico centro di tipo gateway
 * 
 * @author Pierdicca Luca
 * @author Pizzi Massimo
 */
public abstract class Gateway extends Center implements Serializable  {
	
	private static final long serialVersionUID = 1;
	LinkedList<Job> queue;

	
	
	public Gateway() {
		
		super();
		queue = new LinkedList<Job>();
	}
	
	
	@Override
	public abstract double calcCenterDelayTime();	
	
	
	@Override
	public abstract void endOfService(Framework impianto);
	
}