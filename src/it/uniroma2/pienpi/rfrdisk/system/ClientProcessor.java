package it.pierdiccapizzi.system;

import java.io.Serializable;
import it.pierdiccapizzi.eventssequentiator.Clock;
import it.pierdiccapizzi.generators.Exponential;



/** 
 * Implementa un Pc lato client
 * 
 * @author Pierdicca Luca
 * @author Pizzi Massimo
 */
public class ClientProcessor extends Center implements Serializable {
	
	private static final long serialVersionUID = 1;
	Exponential pLaw;
	public static int specificCounter = 0;
	int exaustedJobsCounter = 0;

	
	
	public ClientProcessor() {
		
		super();
		
		specificId = specificCounter;
		specificCounter++;
		typeId = Parameters.CCPUID;
		
		type = CenterType.ProcessoreCL;
		lawType = CenterLawType.Esponenziale;
		lawAverage = Parameters.CCPUAVG;
			
		pLaw = new Exponential(seed, lawAverage);
		
		System.out.println(this.getType() + "\t\tId: " + typeId + " ; " + specificId);
		System.out.println("Seme " + this.seed + "\t\tIn " + this.attemptNum + " tentativi\n");
	}

	
	@Override
	public double calcCenterDelayTime()
	{
		double d = pLaw.generateNextValue();
		if(Parameters.stampa)System.out.println(this.type+"\t"+d);
		return d;
	}

	
	@Override
	public void endOfService(Framework impianto) {
		Center nextCenter;

		nextCenter = impianto.getCenter(typeId+1, 0);		// ...il prossimo centro � la client lan

		nextCenter.setCurrentJob(this.currentJob);			// inserisci il job in un servente (setCurrentJob � overridata per gli ogetti Network e ha la stessa firma)
		((ClientLan)nextCenter).lanClLaw.setAverage(Parameters.CLANAVGClass1);

		((Network) nextCenter).setNewTime(currentJob.getId(), Clock.time + nextCenter.calcCenterDelayTime());		//il tempo nel prossimo centro viene calendarizzato

		this.setCurrentJob(null);
		this.setNewTime(Parameters.INFINITE);
	}
}
