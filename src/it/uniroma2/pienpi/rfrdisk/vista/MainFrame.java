package it.pierdiccapizzi.vista;

import it.pierdiccapizzi.eventssequentiator.Scheduler;
import it.pierdiccapizzi.run.Start;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.AbstractAction;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

import javax.swing.Action;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

public class MainFrame {

	public static JFrame mainFrame;
	
	public static OutputConsolleFrame consolleFrame;
	
	StabConfFrame stabSettingsFrame;
	
	StatConfFrame statSettingsFrame;
	
	JButton stabilization, statistical, density;

	JPanel buttons;

	ImageIcon eqn;
	Image scaledEqn;
	JLabel wallpaper;
	JLabel sfondo;
	
	public static boolean belongsToStab = false; //true = stabConfFrame  false = statConfFrame  


	public MainFrame() {
		
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		mainFrame = new JFrame("RFrDisk - Pierdicca & Pizzi");									// il titolo del frame
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);								// l'azione del pulsante di uscita
		mainFrame.setSize(1024, 600);															// dimensione iniziale
		mainFrame.setLocationRelativeTo(null);													// posizione centrale
		mainFrame.setLayout(new BorderLayout());												// suddivisione del frame
		mainFrame.setResizable(false);
		mainFrame.setVisible(true);																// mostra il frame

		buttons = new JPanel();
		buttons.setLayout(new FlowLayout());
		stabilization = new JButton("Stabilization");
		setStabAcLi();
		buttons.add(stabilization);
		statistical = new JButton("Statistical");
		setStatAcLi();
		buttons.add(statistical);
		
		mainFrame.add(buttons, BorderLayout.SOUTH);

		ImageIcon eqn = new ImageIcon(System.getProperty("user.dir")+"/eqnframe.png");								// recupera l'immagine dell'eqn dal pacchetto
		scaledEqn = eqn.getImage().getScaledInstance(1024, 419, Image.SCALE_DEFAULT);				// adatta l'immagine alla risoluzione desiderata
		eqn.setImage(scaledEqn);																		// 
		wallpaper = new JLabel(eqn);	// crea una etichetta in cui imposta l'immagine
		wallpaper.setBorder(new TitledBorder("EQN"));

		mainFrame.add(wallpaper, BorderLayout.CENTER);													// aggiunge l'etichetta come sfondo centrale

		mainFrame.validate();
		
		mainFrame.pack();
		
		stabSettingsFrame = new StabConfFrame();
		
		statSettingsFrame = new StatConfFrame();
		
		consolleFrame = new OutputConsolleFrame(stabSettingsFrame, statSettingsFrame);
		
		redirectSystemStreams();
		
	}
	
	private void updateTextArea(final String text) {

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				consolleFrame.consolleArea.append(text);
			}
		});
	}

	
	private void redirectSystemStreams() {
		
		OutputStream out = new OutputStream() {
			@Override
			public void write(int b) throws IOException {
				updateTextArea(String.valueOf((char) b));
			}

			@Override
			public void write(byte[] b, int off, int len) throws IOException {
				updateTextArea(new String(b, off, len));
			}

			@Override
			public void write(byte[] b) throws IOException {
				write(b, 0, b.length);
			}
		};

		System.setOut(new PrintStream(out, true));
		System.setErr(new PrintStream(out, true));
	}



	public JFrame getMainFrame() {

		return mainFrame;
	}



	public JButton getStabButton () {

		return stabilization;
	}

	
	public void setStabAcLi(){

		stabilization.addActionListener(new ActionListener()  {

			public void actionPerformed(ActionEvent e) {

				mainFrame.setVisible(false);
				stabSettingsFrame.stabConfFrame.setVisible(true);
			}
		});
	}
	
	
	public void setStatAcLi(){

		statistical.addActionListener(new ActionListener()  {

			public void actionPerformed(ActionEvent e) {

				mainFrame.setVisible(false);
				statSettingsFrame.statConfFrame.setVisible(true);
			}
		});
	}
	
}

