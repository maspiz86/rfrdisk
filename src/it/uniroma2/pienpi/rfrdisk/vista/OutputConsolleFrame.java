package it.pierdiccapizzi.vista;

import it.pierdiccapizzi.run.Start;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

public class OutputConsolleFrame extends JFrame {

	public static JFrame consolle = new JFrame("RFrDisk simulation tool");

	JButton back, menu;

	JPanel buttons;
	
	StabConfFrame stabToInter;
	
	StatConfFrame statToInter;

	static JTextArea consolleArea = new JTextArea();

	public static JProgressBar loading = new JProgressBar();


	
	public OutputConsolleFrame(StabConfFrame stabToInter, StatConfFrame statToInter) {
		
		this.stabToInter = stabToInter;
		this.statToInter = statToInter;
		
		consolle.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		consolle.setSize(600, 700);
		consolle.setLocationRelativeTo(null);
		consolle.setLayout(new BorderLayout());
		consolle.setResizable(false);
		consolle.setVisible(false);

		loading.setIndeterminate(true);
		consolle.add(loading, BorderLayout.NORTH);		

		buttons = new JPanel();
		buttons.setLayout(new FlowLayout());


		back = new JButton("Back");
		buttons.add(back);
		setBackOutputConsolleFrameAcLi();
		
		consolle.add(buttons, BorderLayout.SOUTH);

		consolleArea.setEditable(false);
		JScrollPane scroller = new JScrollPane(consolleArea);
		consolle.add(scroller, BorderLayout.CENTER);

		consolle.validate();		
	}
	
	
	void setBackOutputConsolleFrameAcLi() {

		back.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				
				MainFrame.consolleFrame.consolle.setVisible(false);

				if(MainFrame.belongsToStab) {
					StabConfFrame.stabConfFrame.setVisible(true);
					if(stabToInter.getStabilizationT().isAlive())
						stabToInter.getStabilizationT().stop();
				}
				else{
					StatConfFrame.statConfFrame.setVisible(true);
					if(statToInter.getStatisticT().isAlive()) 
						statToInter.getStatisticT().stop();
				}
				
				MainFrame.mainFrame.setVisible(false);
			}
		});
	}
}
