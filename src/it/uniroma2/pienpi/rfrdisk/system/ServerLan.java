package it.pierdiccapizzi.system;

import java.io.Serializable;
import it.pierdiccapizzi.eventssequentiator.Clock;
import it.pierdiccapizzi.eventssequentiator.Scheduler;
import it.pierdiccapizzi.generators.KErlang;
import it.pierdiccapizzi.generators.RandomSequence;
import it.pierdiccapizzi.generators.Uniform;



/** 
 * Implementa una LAN lato server
 * 
 * @author Pierdicca Luca
 * @author Pizzi Massimo
 */
public class ServerLan extends Network implements Serializable {
	
	private static final long serialVersionUID = 1;
	double parameterKerl;
	KErlang lanSeLaw;
	public static int specificCounter = 0;
	
	public RandomSequence linConServerChoice;
	Uniform unif;
	
	
	
	public ServerLan() {
		
		super();
		
		linConServerChoice = new RandomSequence(this.seed);
		unif = new Uniform(this.seed, linConServerChoice);

		typeId = Parameters.SLANID;
		specificId = specificCounter;
		specificCounter++;
	
		type = CenterType.LanSE;
		lawType = CenterLawType.Kerlangiana;
		
		parameterKerl = Parameters.SLANPAR;
		lanSeLaw = new KErlang(seed, lawAverage, parameterKerl);	
		
		System.out.println(this.getType() + "\t\t\tId: " + typeId + " ; " + specificId);
		System.out.println("Seme " + this.seed + "\t\tIn " + this.attemptNum + " tentativi\n");
	}

	
	@Override
	public double calcCenterDelayTime() {
		
		double d = lanSeLaw.generateNextValue();
		if(Parameters.stampa)System.out.println(this.type+"\t"+d);
		return d;
	}

	
	@Override
	public void endOfService(Framework impianto) {
		
		Center nextCenter;
		currentJob = servants[Scheduler.specificId];										//settare job in questione individuato dal servente
		
		if(this.currentJob.isOutwarding()) {													//se il job � in andata
		
			double temp = unif.generateNextValue();												 //numero compreso tra [0 e 1)
			int serverChoice = (int) (temp*Parameters.serverNum);							 //genero un num int comreso tra 0 e 2 
			
			nextCenter = impianto.getCenter(typeId+1, serverChoice);							//il prossimo centro sar� uno dei server processor

			if(nextCenter.isOccupied)															//se il server processor � occupato...
				((ServerProcessor) nextCenter).queue.add(this.currentJob);							//...mettere il job nella sua coda
			else {																				//se il server processor � libero...
			
				nextCenter.setCurrentJob(this.currentJob);											//...servire direttamente il job in questione
				nextCenter.setNewTime(Clock.time + nextCenter.calcCenterDelayTime());				//...aggiornare il calendario calcolando il tempo di servizio
			}	
		}
		else {																				//se il job � in ritorno...
				
			nextCenter = impianto.getCenter(typeId-1, 0);										//...il prossimo centro sar� il gateway lato server

			if(nextCenter.isOccupied)															//se il gateway � occupato...
				((Gateway) nextCenter).queue.add(this.currentJob);									//...mettere il job nella coda del gateway
			else {																			//se il gateway lato server � libero...
				
				nextCenter.setCurrentJob(this.currentJob);											//...il sever processor servir� il job in questione
				nextCenter.setNewTime(Clock.time + nextCenter.calcCenterDelayTime());				//...aggiornare il calendario calcolando il tempo di servizio
			}
		}
		this.servants[currentJob.getId()] = null;												//liberare il servente 
		this.setNewTime(currentJob.getId(), Parameters.INFINITE);
	}
}

