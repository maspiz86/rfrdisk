package it.pierdiccapizzi.system;

import java.io.Serializable;


/** 
 * Descrive il tipo di disciplina che regola una coda dell'impianto
 * 
 * @author Pierdicca Luca
 * @author Pizzi Massimo
 */
public enum QueueTypes implements Serializable {
	
	Random, Lifo
}
