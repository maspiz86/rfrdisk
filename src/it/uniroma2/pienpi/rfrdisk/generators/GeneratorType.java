package it.pierdiccapizzi.generators;

import java.io.Serializable;

public enum GeneratorType implements Serializable 
{
	Random, Uniform, Exponential, Hyperexponential, Kerlang
}

