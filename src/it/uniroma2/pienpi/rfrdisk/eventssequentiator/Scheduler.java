package it.pierdiccapizzi.eventssequentiator;

import java.text.NumberFormat;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.net.URISyntaxException;
import java.net.URL;

import it.pierdiccapizzi.generators.Generator;
import it.pierdiccapizzi.observationselaborator.Elaborator;
import it.pierdiccapizzi.system.*;
import it.pierdiccapizzi.vista.ResultsFrame;


/** 
 * La classe Scheduler rappresenta il concetto di Sequenziatore in simulazione.
 * Lo Scheduler adempie al compito della gestione e sincronizzazione della simulazione 
 * tramite l'uso di diverse funzioni e di entit� di tipo Framework e EventsCalendar.
 * 
 * @author Pierdicca Luca
 * @author Pizzi Massimo
 */
public class Scheduler {

	public static EventsCalendar currentCalendar;
	Framework currentFramework;

	Elaborator estimator;
	Center currentCenter;
	Center lastCenter;

	
	double gordonAvg;
	double gordonVar;
	public static int typeId;
	public static int specificId;
	public static int currentBatchLength;
	public static boolean flag = true;

	
	
	public Scheduler(){
		currentCalendar = null;
		currentFramework = null;
		estimator = null;
	}


	
	/**
	 * Costruisce un tipo Scheduler. 
	 * 
	 * @param calendarioCorrente		Calendario tramite cui lo Scheduler effettuer� la sincronizzazione della simulazione
	 * @param impiantoCorrente			Impianto su cui lo Scheduler effettuer� la gestione 
	 */
	public Scheduler(EventsCalendar calendarioCorrente, Framework impiantoCorrente) {

		currentCalendar = calendarioCorrente;
		currentFramework = impiantoCorrente;
		estimator = currentFramework.getEstimator();
	}

	
	/**
	 * Restituisce l'Impianto che lo Scheduler sta correntemente gestendo.
	 * 
	 * @return		Impianto gestito dallo Scheduler
	 */
	public Framework getCurrentFramework() {
		return currentFramework;
	}

	
	/** 
	 * Associa un Impianto da gestire allo Scheduler.
	 * 
	 * @param impiantoCorrente		Impianto da gestire
	 */
	public void setCurrentFramework(Framework impiantoCorrente) {
		currentFramework = impiantoCorrente;
		estimator = new Elaborator();
		estimator.subtrahend = new double[impiantoCorrente.getTerminals().size()];
		currentFramework.setEstimator(estimator);
	}


	/**
	 * Associa un Calendario con cui sincronizzare allo Scheduler.
	 * 
	 * @param calendarioCorrente		Calendario con cui sincronizzare
	 */
	public void setCurrentCalendar(EventsCalendar calendarioCorrente) {
		currentCalendar = calendarioCorrente;
	}

	
	/**
	 * Restituisce sotto forma di tempo di scadenza il prossimo evento in Calendario 
	 * ovvero il tempo minore fra tutti.
	 * 
	 * @return 		Tempo di scadenza
	 */
	public Double getMinTimeValue(){

		double minTimeValue = (Double) currentCalendar.centerType.get(0).get(0); //scelgo il primo valore dell'arraylist dei terminali
		int typeIndex, specificIndex, tempSize;

		for(typeIndex = 0 ; typeIndex < currentCalendar.centerType.size() ; typeIndex++) //primo ciclo: cicla sull'arraylist centertype (arraylist di arraylist)
		{ 
			tempSize = currentCalendar.centerType.get(typeIndex).size(); //� la size di ogni arraylist (ogni volta cambia) contenuto nell'arraylist type

			for(specificIndex=0; specificIndex < tempSize; specificIndex++)//secondo ciclo: cicla in ogni arraylist contenuto in centertype
			{	

				if((Double)currentCalendar.centerType.get(typeIndex).get(specificIndex) <= minTimeValue) //faccio un controllo sul valore di minimo 
				{		
					minTimeValue = (Double)currentCalendar.centerType.get(typeIndex).get(specificIndex); //se trovo un valore pi� piccolo del minimo corrente, il minimo corrente lo setto a quel valore
					typeId = typeIndex;    //setto typeId con l'indice di tipo di quel valore
					specificId = specificIndex; //setto specificId con l'indice specifico di quel valore
				}
			}
		}


		return minTimeValue;
	}


	/**
	 * Esegue l'evento in scadenza.
	 * 
	 * @return 		Centro di cui dovr� essere eseguita la EOS
	 */
	public Center exeNextElapsedEvent(){

		Clock.time = getMinTimeValue();
		currentCenter = currentFramework.getCenter(typeId, specificId);
		currentCenter.endOfService(currentFramework);

		return currentCenter;
	}


	/**
	 * Esegue la stabilizzazione dell'Impianto sul tempo medio globale di risposta visto dai Terminali 
	 * tramite la chiamata delle routine di fine evento e l'esecuzione delle opportune osservazioni.
	 * 
	 * @return 		Impianto stabilizzato
	 * 
	 * @throws IOException
	 */
	public Framework runStab(int numeroOsservazioni) throws IOException {

		int observationsToDo = 1;

		gordonAvg = 0L;
		gordonVar = 1L;

		while(observationsToDo <= numeroOsservazioni)		
		{	
			for(int i=0 ; i<Parameters.pilotRunNum ; i++){	

				currentFramework.reset();
				currentCalendar.reset();

				for(int j=0 ; j<Parameters.clientNum ; j++)
					currentFramework.getTerminals().get(j).jobApproach();

				Framework.stabRttObservationsAchieved = 0;

				while(Framework.stabRttObservationsAchieved < observationsToDo){
					currentCenter = exeNextElapsedEvent();
					estimator.observeRttStab(currentCenter);
				}

				estimator.observationsAvgCalc();
				estimator.observations.clear();
			}

			observationsToDo++;
			gordonAvg = estimator.gordonAvgCalc();						
			gordonVar = estimator.gordonVarCalc(gordonAvg);
			estimator.observationsAvg.clear();

		}

		currentFramework.setNzero(Framework.stabRttObservationsAchieved);
	
		System.out.println("\nGordon mean: " + gordonAvg);
		
		new ResultsFrame(gordonAvg);
		
		return currentFramework;
	}


	/**
	 *Tramite la chiamata alla funzione runStab() effettua la generazione e la serializzazione degli stati stabili.
	 * 
	 * @param numeroClient				Numero di Client nell'Impianto
	 * @param numeroRunPilota			Numero di run pilota
	 * @param numeroOsservazioni		Numero massimo di osservazioni
	 * 
	 * @throws IOException
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws SecurityException 
	 * @throws NoSuchMethodException 
	 */
	public void stabilizeOnTerminalsResponseTime(int numeroClient, int numeroRunPilota, int numeroOsservazioni) throws IOException{

		ObjectOutputStream outputFrame, outputCalendar;
		FileOutputStream fileOutputFrame, fileOutputCalendar;
		
		Parameters.clientNum = numeroClient;
		Parameters.pilotRunNum = numeroRunPilota;
		
		Framework impianto = new Framework();
		EventsCalendar calendario = new EventsCalendar();

		long startTime = System.currentTimeMillis();

		this.setCurrentFramework(impianto);
		this.setCurrentCalendar(calendario);

		System.out.println("\n##### AVVIO DELLA STABILIZZAZIONE #####\n");
		
		GregorianCalendar now = new GregorianCalendar();
		
		System.out.println(now.getTime());
		
		Framework steadyFramework = this.runStab(numeroOsservazioni);

		now = new GregorianCalendar();
		System.out.println("\n" + now.getTime());
		
		// stampa info di servizio
		double elapsedTime = (System.currentTimeMillis()- startTime)/1000;
		double second = elapsedTime%60;
		elapsedTime = (elapsedTime-second)/60;
		double minute = elapsedTime%60;
		elapsedTime = (elapsedTime-minute)/60;
		double hour = elapsedTime%24;
		elapsedTime = (elapsedTime-hour)/24;
		double day = elapsedTime%24;
				
		System.out.println("\nTempo impiegato per la stabilizzazione: "+ (int) day +":"+ (int) hour +":"+ (int) minute +":"+ (int) second+ "\n");
	}

	
	/**
	 * Esegue la stima del parametro tempo medio globale di risposta visto dai Terminali
	 * tramite la chiamata delle routine di fine evento e l'esecuzione delle opportune osservazioni.
	 * 
	 * 
	 * @throws IOException
	 */
	public void runTermStat() throws IOException {

		int n = 0;
		double mu = 1.645;
		double delta = 1;
		double sampleVar = 2;
		double sampleMean = 1;
		int nZero = currentFramework.getNzero();
		int term = currentFramework.getTerminals().size();
		Parameters.globalTermBatchNum = 30;
		
		System.out.println("\n##### AVVIO DELLA STIMA #####\n");
		System.out.println("Numero dei client nell'impianto: "+ term);
		System.out.println("\nNumero di osservazioni effettuate per la stabilizzazione: "+nZero);
		System.out.println("\nNumero dei batch eseguiti per la prima stima: "+ Parameters.globalTermBatchNum);

		currentBatchLength = nZero;

		
		while(delta/sampleMean >= 0.2){

			for(int i=0; i<Parameters.globalTermBatchNum;i++){

				Framework.statRttObservationsAchieved = 0;

				while(Framework.statRttObservationsAchieved < currentBatchLength){
					currentCenter = exeNextElapsedEvent();
					estimator.observeRttStat(currentCenter);
				}
			}

			sampleMean = estimator.observationsAvgCalc();
			sampleVar = estimator.sampleVarCalc();
			estimator.observations.clear();
			estimator.observationsAvg.clear();

			delta = mu*(Math.sqrt(sampleVar)/Math.sqrt(Parameters.globalTermBatchNum));

			Parameters.globalTermBatchNum = Parameters.globalTermBatchNum+1;
		}


		System.out.println("Numero dei batch eseguiti per l'ultima stima: "+((Parameters.globalTermBatchNum)-1));
		System.out.println("\nVarianza campionaria: "+sampleVar);
		System.out.println("Media campionaria: "+sampleMean);
		System.out.println("\n" + (sampleMean-delta)+"  <=  E[x]  <=  "+(sampleMean+delta)+"\n");
	}

	
	/**
	 * Esegue la stima del parametro tempo medio globale di risposta visto dai Terminali 
	 * per ogni Impianto precedentemente serializzato. 
	 * 
	 * @param numeroClient		Numero di Client nell'Impianto
	 * 
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws URISyntaxException 
	 */
	public void estimateGlobalAverageResponseTimeOnTerminals(int numeroClient) throws IOException, ClassNotFoundException, URISyntaxException{

		Framework stabedFrameworkFromFile;
		EventsCalendar stabedCalendarFromFile;

		ObjectInputStream inputFramework, inputCalendar;
		FileInputStream fileInputFramework, fileInputCalendar;
		
		fileInputCalendar = new FileInputStream(System.getProperty("user.dir")+"/SSCalendars/StabStateCalendar "+numeroClient+".dat");
		inputCalendar = new ObjectInputStream(fileInputCalendar);

		fileInputFramework = new FileInputStream(System.getProperty("user.dir")+"SSFrameworks/StabStateFramework "+numeroClient+".dat");
		inputFramework = new ObjectInputStream(fileInputFramework);

		stabedCalendarFromFile = (EventsCalendar) inputCalendar.readObject();
		stabedFrameworkFromFile =  (Framework) inputFramework.readObject();

		inputCalendar.close();
		inputFramework.close();

		this.setCurrentCalendar(stabedCalendarFromFile);
		this.setCurrentFramework(stabedFrameworkFromFile);

		this.runTermStat();
	}


}