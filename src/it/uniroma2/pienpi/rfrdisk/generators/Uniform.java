package it.pierdiccapizzi.generators;

import java.io.Serializable;
import it.pierdiccapizzi.system.Parameters;


/** 
 * Implementa un generatore di numeri pseudo casuali con distribuzione uniforme 
 * 
 * @author Pierdicca Luca
 * @author Pizzi Massimo
 */
public class Uniform extends Generator implements Serializable {

	private static final long serialVersionUID = 1;
	RandomSequence linCon;

	
	
	public Uniform(int seme, RandomSequence random) {
		super(seme, 0.0);
		linCon = random;
		type = GeneratorType.Uniform;
	}

	
	@Override
	public double generateNextValue(){
		return  (linCon.generateNextValue()/Parameters.MODULE);
	}


}
