package it.pierdiccapizzi.system;

import it.pierdiccapizzi.eventssequentiator.Job;
import it.pierdiccapizzi.eventssequentiator.Scheduler;
import java.io.Serializable;


/** 
 * Rappresenta una generica unit� funzionale dell'impianto
 * 
 * @author Pierdicca Luca
 * @author Pizzi Massimo
 */
public abstract class Center implements Serializable {

	private static final long serialVersionUID = 1;
	CenterType type;
	CenterLawType lawType;
	int seed = 0;
	boolean isOccupied;
	double lawAverage;
	Job currentJob;
	int typeId;
	int specificId;
	int seedLocation;
	int attemptNum = 0;

	
	
	public Center() {
		
		this.setRandomSeed();
	}

	
	/**
	 * Assegna al centro un seme preso a caso dalla lista dei semi disponibili
	 */
	public void setRandomSeed() {
		
		while(seed == 0) {

			seedLocation = (int) (669 * Math.random());

			seed = Parameters.SEEDS[seedLocation];
			attemptNum ++;
		}
		Parameters.SEEDS[seedLocation] = 0;	
	}

	
	/**
	 * Imposta il seme desiderato
	 * 
	 * @param seme
	 */
	public void setDefinedSeed(int seme){
		seed = seme;
	}

	
	/**
	 * Restituisce il job presente nel centro
	 * 
	 * @return	Il job
	 */
	public Job getCurrentJob() {
		return currentJob;
	}

	
	/**
	 * Imposta il JOB per il centro e assegna il flag di stato che designa un centro come libero od occupato
	 * 
	 * @param jobCorrente
	 */
	public void setCurrentJob(Job jobCorrente) {
		currentJob = jobCorrente;
		if (jobCorrente == null)
			isOccupied = false;
		else
			isOccupied = true;
	}

	
	/**
	 * Restituisce il flag che indica se il centro � occupato o libero
	 * 
	 * @return Il flag
	 */
	public boolean getOccupation() {
		return isOccupied;
	}

	
	/**
	 * Imposta il flag che segna un centro occupato o libero
	 * 
	 * @param occupatoFlag
	 */
	public void setOccupation(boolean occupatoFlag) {
		isOccupied = occupatoFlag;
	}

	
	/**
	 * Restituisce il tipo di legge che governa il centro in questione
	 * 
	 * @return La legge
	 */
	public CenterLawType getLawType() {
		return lawType;
	}

	
	/**
	 * Restituisce la tipologia del centro in questione
	 * 
	 * @return La tipologia
	 */
	public CenterType getType() {
		return type;
	}

	
	/**
	 * Restituisce il numero che identifica il centro tra tutti quelli della stessa tipologia
	 * 
	 * @return L'identificativo
	 */
	public int getSpecificId() {
		return specificId;
	}

	public void setSpecificId(int specificId) {
		this.specificId = specificId;
	}

	
	/**
	 * Restituisce il numero che identifica la tipologia del centro nell'impianto
	 * 
	 * @return L'identificativo
	 */
	public int getTypeId() {
		return typeId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

	
	/**
	 * Imposta il desiderato tempo di clock nel centro
	 * 
	 * @param tempo
	 */
	@SuppressWarnings("unchecked")
	public void setNewTime(double tempo) {
		
		Scheduler.currentCalendar.centerType.get(typeId).set(specificId, tempo);
	}

	
	/**
	 * La routine di fine evento
	 * 
	 * @param impianto
	 */
	public abstract void endOfService(Framework impianto);

	
	/**
	 * Calcola il tempo che impiegher� il centro a servire il job corrente
	 * 
	 * @return		Il tempo
	 */
	public abstract double calcCenterDelayTime();

}
