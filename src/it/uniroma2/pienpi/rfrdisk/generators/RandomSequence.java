package it.pierdiccapizzi.generators;

import it.pierdiccapizzi.system.Parameters;

import java.io.Serializable;


/** 
 * Implementa il metodo della congruenza lineare su cui si baseranno i generatori
 * 
 * @author Pierdicca Luca
 * @author Pizzi Massimo
 */
public class RandomSequence extends Generator implements Serializable {
	
	private static final long serialVersionUID = 1;
	double xn = 0L;											//elemento casuale corrente
	double a = Parameters.MULTIPLIER; 						//moltiplicatore
	double m = Parameters.MODULE	; 						//modulo

	
	
	public RandomSequence(int seme) {
		super(seme, 0.0);
		xn = seed;
		type = GeneratorType.Random;
	}
	
	
	@Override
	public double generateNextValue() {
		xn = (a*xn) % (m);
		return xn;
	}
}
