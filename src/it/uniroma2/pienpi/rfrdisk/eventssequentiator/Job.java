package it.pierdiccapizzi.eventssequentiator;

import java.io.Serializable;


/**
 * La classe Job rappresenta il punto di lavoro di ogni singola unit� funzionale.
 * 
 * @author Pierdicca Luca
 * @author Pizzi Massimo
 */
public class Job implements Serializable {
	
	private static final long serialVersionUID = 1;
	
	public boolean routeTypeFlag;
	int id, serialNumber = 0;
	public static int currentBatchCounter = 1;
	

	
	public Job(){
		routeTypeFlag = true;
		serialNumber = currentBatchCounter;
		currentBatchCounter++;
	}
		
	
	
	/**
	 * Determina se il JOB � in andata o di ritorno
	 * 
	 * @return Il boolean che identifica il tipo di viaggio	
	 */
	public boolean isOutwarding(){
		if(this == null) return false;
		return routeTypeFlag;
	}

	
	
	/**
	 * Definisce se il JOB � in andata o in ritorno
	 * 
	 * @param tipoDiViaggio		true per andata e false per ritorno
	 */
	public void setOutwarding(boolean tipoDiViaggio){
		routeTypeFlag = tipoDiViaggio;
	}

	
	
	/**
	 * Restituisce il numero identificativo di un job
	 * 
	 * @return		Il numero identificativo di un Job
	 */
	public int getId(){
		return id;
	}
	
	
	
	/**
	 * Imposta il numero identificativo di un job
	 * 
	 * @param jobIdentit�		Il numero identificativo di un Job
	 */
	public void setId(int jobIdentit�){
		id = jobIdentit�;
	}
}
